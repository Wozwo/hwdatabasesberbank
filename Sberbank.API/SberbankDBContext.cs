﻿using Microsoft.EntityFrameworkCore;
using Sberbank.ModelToDB;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace Sberbank.API
{
    public class SberbankDBContext : DbContext
    {
        public SberbankDBContext(DbContextOptions<SberbankDBContext> options) :
            base(options)
        { }
        public SberbankDBContext() : base() { }

        public DbSet<User> Users { get; set; }
        public DbSet<Credit> Credits { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<CustomersCredit> CustomersCredits { get; set; }
        public DbSet<Basket> Baskets { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=SberbankDB;Integrated Security=True");
        }
    }
}
