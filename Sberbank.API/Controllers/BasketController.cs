﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sberbank.ModelToDB;

namespace Sberbank.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BasketController : ControllerBase
    {
        // GET: api/Basket
        [HttpGet("{id}")]
        public IEnumerable<Basket> Get(int id)
        {
            return GetBasketUser(new User { Id = id });
        } 

        private IEnumerable<Basket> GetBasketUser(User user)
        {
            IEnumerable<Basket> basketUser = null;
            using (var db = new SberbankDBContext())
            {
                basketUser = db.Baskets.Where(x => x.User.Id == user.Id).Select(x => new Basket { Id = x.Id, Product = x.Product, User = x.User }).ToList();
            }
            return basketUser;
        }

        [HttpPost]
        public string Post([FromBody] IEnumerable<Basket> baskets)
        {
            AddBasketProduct(baskets);
            return $"Продукт успешно добавлен в корзину.";
        }

        private void AddBasketProduct(IEnumerable<Basket> baskets)
        {
            using (var db = new SberbankDBContext())
            {
                db.Baskets.AddRange(baskets.Select(basket => new Basket { 
                    User = db.Users.FirstOrDefault(x => x.Id == basket.User.Id), 
                    Product = db.Products.FirstOrDefault(x => x.Id == basket.Product.Id) }));
                db.SaveChanges();
            }
        }

        [HttpDelete("{id}")]
        public string Delete(int id)
        {
            using(var db = new SberbankDBContext())
            {
                db.Baskets.Remove(db.Baskets.FirstOrDefault(x => x.Id == id));
                db.SaveChanges();
            }
            return "Продукт успешно удален из корзины";
        }
    }
}
