﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sberbank.ModelToDB;

namespace Sberbank.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        // GET: api/Products
        [HttpGet]
        public IEnumerable<Product> Get()
        {
            return GetProduct();
        }

        private IEnumerable<Product> GetProduct()
        {
            IEnumerable<Product> products = null;
            using (var db = new SberbankDBContext())
            {
                products = db.Products.ToList();
            }
            return products;
        }
    }
}
