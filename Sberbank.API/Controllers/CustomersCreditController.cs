﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sberbank.ModelToDB;

namespace Sberbank.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersCreditController : ControllerBase
    {
        // GET: api/CustomersCredit
        [HttpGet("{id}")]
        public IEnumerable<CustomersCredit> Get(int id)
        {
            return GetCustomersCredits(new User { Id = id });
        }

        private IEnumerable<CustomersCredit> GetCustomersCredits(User user)
        {
            IEnumerable<CustomersCredit> customersCredits = null;
            using (var db = new SberbankDBContext())
            {
                customersCredits = db.CustomersCredits
                    .Where(x => x.User.Id == user.Id)
                    .Select(x => new CustomersCredit { 
                        Id = x.Id, 
                        User = x.User, 
                        Credit = x.Credit, 
                        CreditTermMonth = x.CreditTermMonth })
                    .ToList();
            }
            return customersCredits;
        }

        // POST: api/CustomersCredit
        [HttpPost]
        public string Post([FromBody] IEnumerable<CustomersCredit> value)
        {
            AddCustomersCredit(value);
            return "Вы успешно взяли кредит";
        }

        private void AddCustomersCredit(IEnumerable<CustomersCredit> credits)
        {
            using (var db = new SberbankDBContext())
            {
                db.CustomersCredits.AddRange(credits.Select(credit => new CustomersCredit { 
                    User = db.Users.FirstOrDefault(x => x.Id == credit.User.Id), 
                    Credit = db.Credits.FirstOrDefault(x => x.Id == credit.Credit.Id), 
                    CreditTermMonth = credit.CreditTermMonth }));
                db.SaveChanges();
            }
        }
    }
}
