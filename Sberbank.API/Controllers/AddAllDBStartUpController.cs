﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sberbank.ModelToDB;

namespace Sberbank.API.Controllers
{
    [ApiController]
    [Route("api/Add")]
    public class AddAllDBStartUpController : ControllerBase
    {
        // GET: api/AddAllDBStartUp
        [HttpGet]
        public void Get()
        {
            AddDataToDB();
            AddCustomersCredit();
            AddBasketProdcts();
        }

        private void AddBasketProdcts()
        {
            using (var db = new SberbankDBContext())
            {
                db.Baskets.AddRange(new List<Basket> { 
                    new Basket {
                    User = db.Users.FirstOrDefault(x => x.Id == 1),
                    Product = db.Products.FirstOrDefault(x => x.Id == 1) }, 
                    new Basket {
                    User = db.Users.FirstOrDefault(x => x.Id == 1),
                    Product = db.Products.FirstOrDefault(x => x.Id == 3)} } );
                db.SaveChanges();
            }
        }

        private void AddCustomersCredit()
        {
            using (var db = new SberbankDBContext())
            {
                db.CustomersCredits.Add(new CustomersCredit { 
                    Credit = db.Credits.FirstOrDefault(x => x.Id == 5), 
                    User = db.Users.FirstOrDefault(x => x.Id == 1), 
                    CreditTermMonth = 36 });
                db.SaveChanges();
            }
        }

        private string AddDataToDB()
        {
            try
            {
                using (var db = new SberbankDBContext())
                {
                    db.Credits.AddRange(new List<Credit> {
                        new Credit {
                            Name = "Ипотека с гос поддержкой для семей с детьми",
                            InterestRate = 5,
                            MounthTermMin = 60,
                            MounthTermMax = 360 },
                        new Credit {
                            Name = "Ипотека на новостройки",
                            InterestRate = 8,
                            MounthTermMin = 60,
                            MounthTermMax = 360 },
                        new Credit {
                            Name = "Ипотека на вторичном рынке",
                            InterestRate = 9,
                            MounthTermMin = 60,
                            MounthTermMax = 360 },
                        new Credit {
                            Name = "Кредит на любые цели",
                            InterestRate = 15,
                            MounthTermMin = 3,
                            MounthTermMax = 60 },
                        new Credit {
                            Name = "Автокредит",
                            InterestRate = 10,
                            MounthTermMin = 6,
                            MounthTermMax = 60 } });

                    db.Products.AddRange(new List<Product> {
                        new Product { Name = "Страхование жизни", Price = 5000, Sale = 0 },
                        new Product { Name = "Страхование недвижимости", Price = 2000, Sale = 0 },
                        new Product { Name = "Возврат налогов", Price = 3000, Sale = 0 },
                        new Product { Name = "Оценка недвижимости", Price = 2000, Sale = 0 } });

                    db.Users.Add(new User { FirstName = "John", SureName = "Doe", Email = "JohnDoe@post.com" });

                    db.SaveChanges();
                }
                return "Первоначальные данные записаны в БД успешно";
            }
            catch
            {
                return "Первоначальные данные небыли записаны в БД";
            }
        }
    }
}
