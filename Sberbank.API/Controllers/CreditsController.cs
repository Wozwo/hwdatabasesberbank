﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sberbank.ModelToDB;

namespace Sberbank.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CreditsController : ControllerBase
    {
        // GET: api/Credits
        [HttpGet]
        public IEnumerable<Credit> Get()
        {
            return GetCreits();
        }

        private IEnumerable<Credit> GetCreits()
        {
            IEnumerable<Credit> credits = null;
            using (var db = new SberbankDBContext())
            {
                credits = db.Credits.ToList();
            }
            return credits;
        }
    }
}
