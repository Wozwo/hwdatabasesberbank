﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Sberbank.ModelToDB;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Sberbank.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginUserController : ControllerBase
    {
        // Post: api/LoginUser
        [HttpPost]
        public string Post([FromBody] User user)
        {
            var userDB = RegistrationUser(user);
            return JsonConvert.SerializeObject(userDB);
        }

        private User RegistrationUser(User user)
        {
            using (var db = new SberbankDBContext())
            {
                var validUser = db.Users.FirstOrDefault(x =>
                x.SureName == user.SureName &
                x.FirstName == user.FirstName &
                x.Email == user.Email);
                if (validUser == null)
                {
                    db.Users.Add(user);
                    db.SaveChanges();
                    validUser = db.Users.FirstOrDefault(x =>
                        x.SureName == user.SureName &
                        x.FirstName == user.FirstName &
                        x.Email == user.Email);
                    return validUser;
                }
                else
                    return validUser;
            }
        }
    }
}
