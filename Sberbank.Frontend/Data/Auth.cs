﻿using Sberbank.ModelToDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sberbank.Frontend.Data
{
    public class Auth
    {
        public User User { get; set; }
        public DateTime TimeAuth { get; set; }
        public static Auth AuthUser { get; set; } = null;
    }
}
