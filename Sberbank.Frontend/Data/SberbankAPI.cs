﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Newtonsoft.Json;
using Sberbank.Frontend.Pages;
using Sberbank.ModelToDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Sberbank.Frontend.Data
{
    public class SberbankAPI
    {
        public static string Status { get; set; } = "Авторизация";

        public async Task<User> AuthUser(User user)
        {
            var http = new HttpClient();
            var json = JsonConvert.SerializeObject(user);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var task = await http.PostAsync(@"http://localhost:51076/api/loginuser", content);
            var userAuth = await task.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<User>(userAuth);
        }

        public async Task<IEnumerable<ModelToDB.Basket>> GetBasket(User user)
        {
            var http = new HttpClient();
            var task = await http.GetAsync($"http://localhost:51076/api/Basket/{user.Id}");
            var baskets = await task.Content.ReadAsStringAsync(); 
            return JsonConvert.DeserializeObject<IEnumerable<ModelToDB.Basket>>(baskets);
        }

        public async Task<IEnumerable<Product>> GetProducts()
        {
            var http = new HttpClient();
            var task = await http.GetAsync($"http://localhost:51076/api/Products");
            var products = await task.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<IEnumerable<Product>>(products);
        }

        public async Task<IEnumerable<Credit>> GetCredits()
        {
            var http = new HttpClient();
            var task = await http.GetAsync($"http://localhost:51076/api/Credits");
            var credits = await task.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<IEnumerable<Credit>>(credits);
        }

        public async Task<string> AddCreditToDB(Credit credit)
        {
            var http = new HttpClient();
            var customCredit = new List<CustomersCredit> { 
                new CustomersCredit { 
                    Credit = credit, 
                    CreditTermMonth = 100, 
                    User = Auth.AuthUser.User } };
            var json = JsonConvert.SerializeObject(customCredit);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var task = await http.PostAsync(@"http://localhost:51076/api/CustomersCredit", content);
            var response = await task.Content.ReadAsStringAsync();
            return response;
        }

        public async Task<IEnumerable<CustomersCredit>> GetMyCredits(User user)
        {
            var http = new HttpClient();
            var task = await http.GetAsync($"http://localhost:51076/api/CustomersCredit/{user.Id}");
            var credits = await task.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<IEnumerable<CustomersCredit>>(credits);
        }

        public async Task<string> AddProdToDB(Product product)
        {
            var http = new HttpClient();
            var customBasket = new List<ModelToDB.Basket> {
                new ModelToDB.Basket{ 
                    Product = product, 
                    User = Auth.AuthUser.User } };
            var json = JsonConvert.SerializeObject(customBasket);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var task = await http.PostAsync(@"http://localhost:51076/api/Basket", content);
            var response = await task.Content.ReadAsStringAsync();
            return response;
        }

        public async Task<string> DeleteProduct(int id)
        {
            var http = new HttpClient();
            var task = await http.DeleteAsync($"http://localhost:51076/api/Basket/{id}");
            var res = await task.Content.ReadAsStringAsync();
            return res;
        }
    }
}
