﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sberbank.ModelToDB
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public int Sale { get; set; }
    }
}
