﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sberbank.ModelToDB
{
    public class CustomersCredit
    {
        public int Id { get; set; }
        public User User { get; set; }
        public Credit Credit { get; set; }
        public int CreditTermMonth { get; set; }
    }
}
