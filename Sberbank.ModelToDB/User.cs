﻿using System;

namespace Sberbank.ModelToDB
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SureName { get; set; }
        public string Email { get; set; }
    }
}
