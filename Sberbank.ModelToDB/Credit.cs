﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sberbank.ModelToDB
{
    public class Credit
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double InterestRate { get; set; }
        public int MounthTermMin { get; set; }
        public int MounthTermMax { get; set; }
    }
}
