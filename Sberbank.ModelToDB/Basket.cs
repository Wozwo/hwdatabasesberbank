﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sberbank.ModelToDB
{
    public class Basket
    {
        public int Id { get; set; }
        public User User { get; set; }
        public Product Product { get; set; }
    }
}
